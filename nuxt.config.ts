// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  ssr: false,
  css: ['~/assets/css/main.css'],
  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },
  build: {
    transpile: ['primevue'],
  },
  modules: ['@sidebase/nuxt-auth', 'nuxt-icon', '@pinia/nuxt'],
  auth: {
    provider: {
      type: 'authjs',
    },
    globalAppMiddleware: false,
    session: {
      enableRefreshPeriodically: 30000,
      enableRefreshOnWindowFocus: false,
    },
  },
  // https://nuxt.com/docs/guide/going-further/runtime-config#environment-variables
  runtimeConfig: {
    public: {
      authIssuer: '',
      keycloakAdminApi: '',
      chatDocumentationUrl: '',
    },
    authOrigin: '',
    authClientId: '',
    authSecret: '',
    authAuthorization: '',
    authWellknown: '',
    nextauthSecret: '',
    matrixBotUrl: '',
    matrixBotSecret: '',
    matrixMediaDownloadUrl: '',
  },
  typescript: {
    strict: true,
  },
  imports: {
    dirs: ['stores'],
  },
  sourcemap: {
    server: true,
    client: true,
  },
});
