# Required env vars for production (and dev)

`PUBLIC_DOMAIN`=the public domain where the app is reachable
`ROTESTEAMBOT_URL`=Baseurl where you can reach deployed https://gitlab.com/rotesteam/rotesteambot
`MATRIX_SERVER`=The domain of a matrix server, which will be used to resolve the avatars of Spaces

```
export AUTH_ORIGIN="https://PUBLIC_DOMAIN/api/auth"
export NEXTAUTH_URL="https://PUBLIC_DOMAIN"
export NUXT_AUTH_CLIENT_ID="KEYCLOAK_CLIENT_ID"
export NUXT_AUTH_SECRET="KEYCLOAK_CLIENT_SECRET"
export NUXT_AUTH_AUTHORIZATION="https://KEYCLOAK.DOMAIN/realms/REALM/protocol/openid-connect/auth"
export NUXT_AUTH_WELLKNOWN="https://KEYCLOAK.DOMAIN/realms/REALM/.well-known/openid-configuration"
export NUXT_PUBLIC_AUTH_ISSUER="https://KEYCLOAK.DOMAIN/realms/REALM"
export NUXT_PUBLIC_KEYCLOAK_ADMIN_API="https://KEYCLOAK.DOMAIN/admin/realms/REALM"
export NUXT_NEXTAUTH_SECRET="SECRET"

export NUXT_MATRIX_BOT_URL="https://ROTESTEAMBOT_URL"
export NUXT_MATRIX_BOT_SECRET="SECRET"
export NUXT_MATRIX_MEDIA_DOWNLOAD_URL="https://MATRIX_SERVER"
```

# Nuxt 3 Minimal Starter

Look at the [Nuxt 3 documentation](https://nuxt.com/docs/getting-started/introduction) to learn more.

## Setup

Make sure to install the dependencies:

```bash
# npm
npm install

# pnpm
pnpm install

# yarn
yarn install
```

## Development Server

Start the development server on `http://localhost:3000`:

```bash
# npm
npm run dev

# pnpm
pnpm run dev

# yarn
yarn dev
```

## Production

Build the application for production:

```bash
# npm
npm run build

# pnpm
pnpm run build

# yarn
yarn build
```

Locally preview production build:

```bash
# npm
npm run preview

# pnpm
pnpm run preview

# yarn
yarn preview
```

Check out the [deployment documentation](https://nuxt.com/docs/getting-started/deployment) for more information.
