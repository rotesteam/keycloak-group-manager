import { useKeycloakHelper } from '~/composables/useKeycloakHelper';
import { InviteLink } from '~/types/invitelink.model';
import { User } from '~/types/user.model';
import { KCGroup } from '../../types/groups.model';

const config = useRuntimeConfig();

function joinToUrl(first: string, second: string): string {
  if (first.endsWith('/')) {
    first = first.substring(0, first.length - 1);
  }
  if (second.startsWith('/')) {
    second = second.substring(1);
  }
  return first + '/' + second;
}

async function botRequest(
  resource: string,
  method: string,
  body: any = undefined,
) {
  if (!config.matrixBotSecret) {
    throw new Error('No NUXT_MATRIX_BOT_SECRET set');
  }
  if (!config.matrixBotUrl) {
    throw new Error('No NUXT_MATRIX_BOT_URL set');
  }
  const url = joinToUrl(config.matrixBotUrl, resource);
  const response = await fetch(url, {
    method,
    headers: {
      SharedSecret: config.matrixBotSecret!,
      ...(body ? { 'Content-Type': 'application/json' } : {}),
    },
    ...(body ? { body: JSON.stringify(body) } : {}),
  });
  if (response.status < 200 || response.status >= 300) {
    throw new Error(
      `Error response ${response.status} - ${
        response.statusText
      } during ${method}-Request from Bot ${url}. Response-body: ${await response.text()}`,
    );
  }
  return response;
}

export interface Space {
  spaceName: string;
  id: string;
  avatarUrl: string | null;
}

export async function getSpacesOfGroup(groupId: string) {
  const response = await botRequest(
    `/groups/${encodeURIComponent(groupId)}/spaces`,
    'GET',
  );
  return ((await response.json()) as Space[]).map(resolveMediaUrl);
}
export interface GroupMemberOverview {
  [index: string]: SpaceMemberOverview;
}
export interface SpaceMemberOverview {
  joinedButNoGroupMember: string[];
  joinedAndGroupMember: string[];
  notJoinedButGroupMember: string[];
}

class MatrixBot {
  constructor() {}

  public async getSpaceMemberOverview(
    kcGroupId: string,
  ): Promise<GroupMemberOverview> {
    const response = await botRequest(
      `/groups/${encodeURIComponent(kcGroupId)}/memberOverview`,
      'GET',
    );
    return (await response.json()) as GroupMemberOverview;
  }

  inviteAll(group: KCGroup) {
    return botRequest(
      `/groups/${encodeURIComponent(group.id)}/inviteAll`,
      'POST',
    );
  }

  inviteUserToSpace(kcUser: string, spaceId: string) {
    return botRequest(
      `/users/${encodeURIComponent(kcUser)}/invite?space=${encodeURIComponent(
        spaceId,
      )}`,
      'POST',
    );
  }

  public async getManagableSpaces(kcUser: string) {
    const response = await botRequest(
      `/users/${encodeURIComponent(kcUser)}/manageableSpaces`,
      'GET',
    );
    return ((await response.json()) as Space[]).map(resolveMediaUrl);
  }

  checkUser(kcUser: string) {
    return botRequest(`/users/${encodeURIComponent(kcUser)}/check`, 'POST');
  }

  public async createNewSpace(kcUser: string, spaceName: string) {
    const response = await botRequest(
      `/users/${encodeURIComponent(
        kcUser,
      )}/newSpace?spaceName=${encodeURIComponent(spaceName)}`,
      'POST',
    );
    return resolveMediaUrl((await response.json()) as Space);
  }

  public async prepareSpace(spaceId: string): Promise<string> {
    const response = await botRequest(
      `/spaces/${encodeURIComponent(spaceId)}/prepare`,
      'POST',
    );
    return await response.text();
  }

  public informInviteLinkUsed(
    usedBy: User,
    invite: InviteLink,
    group: KCGroup,
    informKCIDs: string[],
  ) {
    const inviteLabel = encodeURIComponent(invite.label);
    const userDisplayName = encodeURIComponent(
      useKeycloakHelper().getUserName(usedBy),
    );
    const groupName = encodeURIComponent(group.name);
    return botRequest(
      `/notify/inviteUsed?inviteLabel=${inviteLabel}&usedByName=${userDisplayName}&groupName=${groupName}`,
      'POST',
      informKCIDs,
    );
  }
}

export const matrixBot = new MatrixBot();

function resolveMediaUrl(space: Space) {
  const configuredUrl = config.matrixMediaDownloadUrl;

  if (configuredUrl && space.avatarUrl) {
    const matches = space.avatarUrl.match(/mxc:\/\/(.+)\/(.+)/i);
    const baseUrl = configuredUrl.endsWith('/')
      ? configuredUrl.substring(0, configuredUrl.length - 1)
      : configuredUrl;
    const resolvedUrl =
      baseUrl + `/_matrix/media/v3/download/${matches![1]}/${matches![2]}`;
    space.avatarUrl = resolvedUrl;
  }
  return space;
}
