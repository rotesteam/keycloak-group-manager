import { useDirectKeycloak } from '~/composables/useDirectKeycloak';
import { useKeycloakHelper } from '~/composables/useKeycloakHelper';
import { KCGroup } from '~/types/groups.model';
import { UserInfo } from '~/types/userInfo.model';

export async function maySearchUsers(user: UserInfo): Promise<boolean> {
  return (
    (await useDirectKeycloak().isAdminSomewhere(user.sub)) ||
    useKeycloakHelper().hasViewUsersRole(user)
  );
}

export async function mayManageGroup(
  user: UserInfo,
  group: KCGroup,
): Promise<boolean> {
  return (
    (await useDirectKeycloak().isAdmin(user.sub, group)) || isGlobalAdmin(user)
  );
}

export function isGlobalAdmin(user: UserInfo) {
  return useKeycloakHelper().hasManageUserRole(user);
}

export async function mayViewGroup(
  user: UserInfo,
  group: KCGroup,
): Promise<boolean> {
  return (
    (await useDirectKeycloak().isMember(user.sub, group.id)) ||
    useKeycloakHelper().hasManageUserRole(user)
  );
}
