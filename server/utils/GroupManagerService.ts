import { useDirectKeycloak } from '~/composables/useDirectKeycloak';
import { KCGroup } from '~/types/groups.model';
import { InviteLink } from '~/types/invitelink.model';
import { UserInfo } from '~/types/userInfo.model';
import { isGlobalAdmin } from './permissions';

/**
 * Enthält die Business-Logik, die für Änderungen nötig sind.
 *
 * Lesende Aktionen sind hier nicht.
 * */
export class GroupManagerService {
  constructor(private group: KCGroup) {}

  async addInvite(invite: InviteLink) {
    await useDirectKeycloak().addInvite(this.group, invite);
  }

  async deleteInvite(invite: InviteLink) {
    await useDirectKeycloak().deleteInvite(this.group, invite);
  }

  async updateInvite(invite: InviteLink) {
    await useDirectKeycloak().updateInvite(this.group, invite);
  }

  async changeAdmins(adminsToAdd: string[], adminsToRemove: string[]) {
    return useDirectKeycloak().changeAdmins(
      this.group,
      adminsToAdd,
      adminsToRemove,
    );
  }

  async changeUsers(usersToAdd: string[], usersToRemove: string[]) {
    const kcAPI = useDirectKeycloak();

    for (const newUser of usersToAdd) {
      await kcAPI.joinGroup(newUser, this.group.id);
      await matrixBot.checkUser(newUser);
    }
    for (const removeUser of usersToRemove) {
      await kcAPI.leaveGroup(removeUser, this.group.id);
    }
    if (usersToAdd.length > 0 || usersToRemove.length > 0) {
      if (kcAPI.isCompositeParent(this.group)) {
        await this.syncCompositeMembership();
      }
      for (const parent of kcAPI.getCompositeParents(this.group)) {
        await new GroupManagerService(
          await kcAPI.getGroup(parent),
        ).syncCompositeMembership();
      }
    }
  }

  private async syncCompositeMembership() {
    const kcAPI = useDirectKeycloak();
    const currentMembers = new Set(
      (await kcAPI.getGroupMembers(this.group.id)).map((m) => m.id),
    );

    const usersInAtLeastOneGroup = (
      await kcAPI.getMembers(kcAPI.getCompositeChildren(this.group))
    ).getUnifiedMembers();

    const missingMembers = Array.from(usersInAtLeastOneGroup.values()).filter(
      (m) => !currentMembers.has(m),
    );
    const excessUsers = Array.from(currentMembers).filter(
      (u) => !usersInAtLeastOneGroup.has(u),
    );
    await this.changeUsers(missingMembers, excessUsers);
  }

  async changeSpaces(
    kcId: string,
    newSpaceIds: string[],
    removeSpaceIds: string[],
  ) {
    if (typeof kcId != 'string') {
      throw new Error('kcId must be string');
    }
    if (newSpaceIds.length > 0) {
      const managableSpaces = await matrixBot.getManagableSpaces(kcId);
      for (const space of newSpaceIds) {
        if (
          !managableSpaces.find((managableSpace) => managableSpace.id == space)
        ) {
          throw new Error(`spaceid ${space} is not managable by the user`);
        }
      }
    }

    for (const newSpace of newSpaceIds) {
      await matrixBot.prepareSpace(newSpace);
    }
    await useDirectKeycloak().changeSpaces(
      this.group,
      newSpaceIds,
      removeSpaceIds,
    );
  }

  async inviteAll() {
    return matrixBot.inviteAll(this.group);
  }

  async createNewSpace(kcId: string, spaceName: string) {
    const space = await matrixBot.createNewSpace(kcId, spaceName);
    await useDirectKeycloak().changeSpaces(this.group, [space.id], []);
  }

  async inviteUserToSpace(kcId: string, spaceId: string) {
    const currentAssignedSpaces = await getSpacesOfGroup(this.group.id);
    if (!currentAssignedSpaces.find((s) => s.id == spaceId)) {
      throw new Error('Space gehört nicht zu Gruppe');
    }
    await matrixBot.inviteUserToSpace(kcId, spaceId);
  }

  async makeNoLongerComposite() {
    const kcApi = useDirectKeycloak();
    if (kcApi.isCompositeParent(this.group)) {
      const users = await kcApi.getGroupMembers(this.group.id);
      await this.changeUsers(
        [],
        users.map((u) => u.id),
      );
      const compChildGroups = kcApi.getCompositeChildren(this.group);
      await kcApi.clearIsCompositeParent(this.group);
      const errors = [];
      for (const childId of compChildGroups) {
        try {
          const childGroup = await kcApi.getGroup(childId);
          if (childGroup) {
            await kcApi.removeCompositeParent(childGroup, this.group);
          }
        } catch (e) {
          // We need to continue regardless, to ensure it's as correct as possible
          errors.push(e);
        }
      }
      if (errors.length > 0) {
        throw new Error('Error while clearing composite-children', {
          cause: errors,
        });
      }
    }
  }

  async removeCompositeChild(child: KCGroup) {
    await useDirectKeycloak().removeCompositeChild(this.group, child);
    await useDirectKeycloak().removeCompositeParent(child, this.group);
    await this.syncCompositeMembership();
  }

  async addCompositeChild(child: KCGroup) {
    await useDirectKeycloak().addCompositeChild(this.group, child);
    await useDirectKeycloak().addCompositeParent(child, this.group);
    await this.syncCompositeMembership();
  }
}

/**
 * Enthält die Business-Logik von schreibenden Funktionalitäten, die nur von Gruppen-Admins ausgeführt werden dürfen
 *
 * */
export class GroupAdminActionService {
  constructor(private groupManagerService: GroupManagerService) {}

  async addInvite(invite: InviteLink) {
    await this.groupManagerService.addInvite(invite);
  }

  async deleteInvite(invite: InviteLink) {
    await this.groupManagerService.deleteInvite(invite);
  }

  async updateInvite(invite: InviteLink) {
    await this.groupManagerService.updateInvite(invite);
  }

  async changeAdmins(adminsToAdd: string[], adminsToRemove: string[]) {
    return this.groupManagerService.changeAdmins(adminsToAdd, adminsToRemove);
  }

  async changeUsers(usersToAdd: string[], usersToRemove: string[]) {
    return this.groupManagerService.changeUsers(usersToAdd, usersToRemove);
  }

  async changeSpaces(
    kcId: string,
    newSpaceIds: string[],
    removeSpaceIds: string[],
  ) {
    return this.groupManagerService.changeSpaces(
      kcId,
      newSpaceIds,
      removeSpaceIds,
    );
  }

  async inviteAll() {
    return this.groupManagerService.inviteAll();
  }

  async createNewSpace(kcId: any, spaceName: string) {
    return this.groupManagerService.createNewSpace(kcId, spaceName);
  }

  async inviteUserToSpace(kcId: string, spaceId: string) {
    return this.groupManagerService.inviteUserToSpace(kcId, spaceId);
  }

  static async getGroupAdminActionService(admin: UserInfo, group: KCGroup) {
    if (await mayManageGroup(admin, group)) {
      return new GroupAdminActionService(new GroupManagerService(group));
    } else {
      return 'unauthorized';
    }
  }
}

export class GlobalAdminActionService {
  constructor(private groupManagerService: GroupManagerService) {}
  public addCompositeChild(child: KCGroup) {
    return this.groupManagerService.addCompositeChild(child);
  }

  public removeCompositeChild(child: KCGroup) {
    return this.groupManagerService.removeCompositeChild(child);
  }

  public makeNoLongerComposite() {
    return this.groupManagerService.makeNoLongerComposite();
  }

  static async getGlobalAdminActionService(
    globalAdmin: UserInfo,
    group: KCGroup,
  ) {
    if (isGlobalAdmin(globalAdmin)) {
      return new GlobalAdminActionService(new GroupManagerService(group));
    } else {
      return 'unauthorized';
    }
  }
}
