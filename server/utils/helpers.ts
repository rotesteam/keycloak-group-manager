import { GroupInfo, KCGroup } from '~/types/groups.model';
import {
  GlobalAdminActionService,
  GroupAdminActionService,
} from './GroupManagerService';

import { EventHandlerRequest, H3Event } from 'h3';
import { useDirectKeycloak } from '~/composables/useDirectKeycloak';

export function accessGroupAdminService<T>(
  event: H3Event<EventHandlerRequest>,
  group: KCGroup,
  adminServiceCallback: (adminService: GroupAdminActionService) => Promise<T>,
): Promise<T | void> {
  return GroupAdminActionService.getGroupAdminActionService(
    event.context.kcUser,
    group,
  ).then((service) => {
    if (service instanceof GroupAdminActionService) {
      return adminServiceCallback(service);
    } else if (service == 'unauthorized') {
      setResponseStatus(event, 403, 'unauthorized');
    }
  });
}

export function accessGlobalAdminService<T>(
  event: H3Event<EventHandlerRequest>,
  group: KCGroup,
  adminServiceCallback: (adminService: GlobalAdminActionService) => Promise<T>,
): Promise<T | void> {
  return GlobalAdminActionService.getGlobalAdminActionService(
    event.context.kcUser,
    group,
  ).then((service) => {
    if (service instanceof GlobalAdminActionService) {
      return adminServiceCallback(service);
    } else if (service == 'unauthorized') {
      setResponseStatus(event, 403, 'unauthorized');
    }
  });
}

export function toGroupInfo(group: KCGroup): GroupInfo {
  return {
    id: group.id,
    name: group.name,
    path: group.path,
    compositeGroup: useDirectKeycloak().isCompositeParent(group),
  };
}
