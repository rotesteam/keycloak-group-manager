import { array, object, string } from 'yup';
import { useDirectKeycloak } from '../../../../composables/useDirectKeycloak';
const ChangeAdminSchema = object({
  removeAdmins: array(string().required()).optional(),
  addAdmins: array(string().required()).optional(),
});
export default defineEventHandler(async (event) => {
  const group = await useDirectKeycloak().getGroup(event.context.params!.group);
  return accessGroupAdminService(event, group, async (service) => {
    const body = await readBody(event);
    if (ChangeAdminSchema.isValidSync(body)) {
      await service.changeAdmins(body.addAdmins ?? [], body.removeAdmins ?? []);
      return 'success';
    } else {
      setResponseStatus(event, 400, 'Body is not valid');
    }
  });
});
