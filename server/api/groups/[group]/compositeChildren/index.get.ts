import { useDirectKeycloak } from '~/composables/useDirectKeycloak';
import { accessGlobalAdminService, toGroupInfo } from '~/server/utils/helpers';

export default defineEventHandler(async (event) => {
  const group = await useDirectKeycloak().getGroup(event.context.params!.group);
  if (!(await mayViewGroup(event.context.kcUser, group))) {
    setResponseStatus(event, 403, 'not allowed to view group');
    return;
  }
  const childrenIds = useDirectKeycloak().getCompositeChildren(group);
  try {
    const children = await Promise.all(
      childrenIds.map((i) => useDirectKeycloak().getGroup(i)),
    );
    return children.map(toGroupInfo);
  } catch (error) {
    throw createError({
      statusCode: 500,
      message: 'Error while fetching parent groups of group ' + group.id,
      cause: error,
    });
  }
});
