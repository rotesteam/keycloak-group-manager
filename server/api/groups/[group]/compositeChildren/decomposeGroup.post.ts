import { useDirectKeycloak } from '~/composables/useDirectKeycloak';
import { accessGlobalAdminService } from '~/server/utils/helpers';

export default defineEventHandler(async (event) => {
  const group = await useDirectKeycloak().getGroup(event.context.params!.group);
  return accessGlobalAdminService(event, group, async (adminService) =>
    adminService.makeNoLongerComposite(),
  );
});
