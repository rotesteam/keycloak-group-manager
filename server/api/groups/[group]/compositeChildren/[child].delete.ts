import { useDirectKeycloak } from '~/composables/useDirectKeycloak';
import { accessGlobalAdminService } from '~/server/utils/helpers';

export default defineEventHandler(async (event) => {
  const group = await useDirectKeycloak().getGroup(event.context.params!.group);
  const child = await useDirectKeycloak().getGroup(event.context.params!.child);
  return accessGlobalAdminService(event, group, async (adminService) =>
    adminService.removeCompositeChild(child),
  );
});
