import { array, object, string } from 'yup';
import { useDirectKeycloak } from '../../../../composables/useDirectKeycloak';

const ChangeSpacesSchema = object({
  spacesToAdd: array(string().required()).optional(),
  spacesToRemove: array(string().required()).optional(),
});

export default defineEventHandler(async (event) => {
  const group = await useDirectKeycloak().getGroup(event.context.params!.group);

  const body = await readBody(event);

  return accessGroupAdminService(event, group, async (service) => {
    if (!ChangeSpacesSchema.isValidSync(body)) {
      setResponseStatus(event, 400, 'body does not match shape');
      return;
    }
    await service.changeSpaces(
      event.context.kcId,
      body.spacesToAdd ?? [],
      body.spacesToRemove ?? [],
    );
    return 'success';
  });
});
