import { useDirectKeycloak } from '~/composables/useDirectKeycloak';
import { GroupInfo, KCGroup } from '~/types/groups.model';

export default defineEventHandler(async (event) => {
  const group = await useDirectKeycloak().getGroup(event.context.params!.group);
  if (!(await mayViewGroup(event.context.kcUser, group))) {
    setResponseStatus(event, 403, 'Not allowed to view Group');
  }
  const groupInfo: GroupInfo = {
    id: group.id,
    name: group.name,
    path: group.path,
    compositeGroup: useDirectKeycloak().isCompositeParent(group),
  };

  return groupInfo;
});
