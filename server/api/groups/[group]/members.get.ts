import { mayViewGroup } from '~/server/utils/permissions';
import { useDirectKeycloak } from '../../../../composables/useDirectKeycloak';

export default defineEventHandler(async (event) => {
  const groupId = event.context.params?.group;

  if (!groupId) {
    setResponseStatus(event, 400);
    return;
  }

  if (
    !(await mayViewGroup(
      event.context.kcUser,
      await useDirectKeycloak().getGroup(groupId),
    ))
  ) {
    setResponseStatus(event, 403);
    return;
  }

  const members = await useDirectKeycloak().getGroupMembers(groupId);
  return members.map((user) => ({
    id: user.id,
    username: user.username,
    firstName: user.firstName,
    lastName: user.lastName,
  }));
});
