import { useDirectKeycloak } from '~/composables/useDirectKeycloak';
import { matrixBot } from '~/server/utils/matrixbot';

export default defineEventHandler(async (event) => {
  const groupId: string = event.context.params!.group;
  const group = await useDirectKeycloak().getGroup(groupId);
  if (await mayViewGroup(event.context.kcUser, group)) {
    return await matrixBot.getSpaceMemberOverview(groupId);
  } else {
    setResponseStatus(event, 403);
  }
});
