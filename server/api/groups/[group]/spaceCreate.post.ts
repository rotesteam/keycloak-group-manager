import { useDirectKeycloak } from '../../../../composables/useDirectKeycloak';

export default defineEventHandler(async (event) => {
  if (!getQuery(event)['spaceName']?.toString()) {
    setResponseStatus(event, 400, 'spaceName not in query param');
    return;
  }
  const group = await useDirectKeycloak().getGroup(event.context.params!.group);

  return accessGroupAdminService(event, group, async (service) => {
    await service.createNewSpace(
      event.context.kcId,
      getQuery(event)['spaceName']!.toString(),
    );
    return 'success';
  });
});
