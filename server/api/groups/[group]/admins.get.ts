import { useDirectKeycloak } from '../../../../composables/useDirectKeycloak';

export default defineEventHandler(async (event) => {
  const group = await useDirectKeycloak().getGroup(event.context.params!.group);
  if (await mayViewGroup(event.context.kcUser, group)) {
    return useDirectKeycloak().getAdmins(group);
  } else {
    setResponseStatus(event, 403);
  }
});
