import { useDirectKeycloak } from '../../../../composables/useDirectKeycloak';

export default defineEventHandler(async (event) => {
  const group = await useDirectKeycloak().getGroup(event.context.params!.group);
  if (!getQuery(event)['space']?.toString()) {
    setResponseStatus(event, 400, 'space not in query param');
    return;
  }
  if (!getQuery(event)['user']?.toString()) {
    setResponseStatus(event, 400, 'user not in query param');
    return;
  }

  return accessGroupAdminService(event, group, async (service) => {
    await service.inviteUserToSpace(
      getQuery(event)['user']!.toString(),
      getQuery(event)['space']!.toString(),
    );
    return 'success';
  });
});
