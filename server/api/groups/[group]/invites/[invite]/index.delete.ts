import { useDirectKeycloak } from '~/composables/useDirectKeycloak';
import { accessGroupAdminService } from '~/server/utils/helpers';

export default defineEventHandler(async (event) => {
  const group = await useDirectKeycloak().getGroup(event.context.params!.group);
  return accessGroupAdminService(event, group, async (service) => {
    const inviteId = event.context.params!.invite;
    const invite = await useDirectKeycloak().getInvite(group, inviteId);
    if (!invite) {
      setResponseStatus(event, 404, 'Invite nicht gefunden');
      return;
    }
    await service.deleteInvite(invite);
    return 'success';
  });
});
