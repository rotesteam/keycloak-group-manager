import { LinkStatus, checkInviteLinkStatus } from '~/composables/inviteLink';
import { useDirectKeycloak } from '~/composables/useDirectKeycloak';
import { ClaimInfo, ClaimStatus } from '~/types/invitelink.model';
import { User } from '~/types/user.model';

export default defineEventHandler(async (event) => {
  const group = await useDirectKeycloak().getGroup(event.context.params!.group);
  const inviteId = event.context.params!.invite;
  const invite = await useDirectKeycloak().getInvite(group, inviteId);
  let user: User | null = null;
  if (event.context.kcId) {
    user = await useDirectKeycloak().getUser(event.context.kcId);
  }

  async function isMemberChecker(): Promise<boolean | undefined> {
    if (user) {
      return await useDirectKeycloak().isMember(user.id, group.id);
    } else {
      return undefined;
    }
  }

  async function determineClaimInfo(): Promise<ClaimInfo> {
    if (!invite) {
      return {
        status: ClaimStatus.NOTCLAIMABLE_OR_MISSING,
        groupName: group.name,
      };
    }

    if (await isMemberChecker()) {
      return { status: ClaimStatus.ALREADY_MEMBER, groupName: group.name };
    }

    if (
      (await checkInviteLinkStatus(invite, isMemberChecker)) ==
      LinkStatus.CLAIMABLE
    ) {
      return { status: ClaimStatus.CLAIMABLE, groupName: group.name };
    } else {
      return {
        status: ClaimStatus.NOTCLAIMABLE_OR_MISSING,
        groupName: group.name,
      };
    }
  }

  return await determineClaimInfo();
});
