import { LinkStatus, checkInviteLinkStatus } from '~/composables/inviteLink';
import { useDirectKeycloak } from '~/composables/useDirectKeycloak';
import { GroupManagerService } from '~/server/utils/GroupManagerService';
import { ClaimStatus } from '~/types/invitelink.model';

export default defineEventHandler(async (event) => {
  const group = await useDirectKeycloak().getGroup(event.context.params!.group);
  const inviteId = event.context.params!.invite;
  const invite = await useDirectKeycloak().getInvite(group, inviteId);
  const user = await useDirectKeycloak().getUser(event.context.kcId);

  if (!user) {
    setResponseStatus(event, 400, 'Benutzer nicht gefunden');
    return;
  }

  if (!invite) {
    setResponseStatus(event, 404, 'InviteLink nicht gefunden');
    return;
  }

  if (
    (await checkInviteLinkStatus(invite, () =>
      useDirectKeycloak().isMember(user.id, group.id),
    )) != LinkStatus.CLAIMABLE
  ) {
    setResponseStatus(
      event,
      400,
      'InviteLink kann von Benutzer nicht eingelöst werden',
    );
    return;
  }
  invite.useCount++;
  invite.usesLeft--;
  await useDirectKeycloak().updateInvite(group, invite);
  await new GroupManagerService(group).changeUsers([user.id], []);
  if (invite.notifications) {
    await matrixBot.informInviteLinkUsed(
      user,
      invite,
      group,
      await useDirectKeycloak().getAdmins(group),
    );
  }
  return 'success';
});
