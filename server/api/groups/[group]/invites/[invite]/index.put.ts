import { useDirectKeycloak } from '~/composables/useDirectKeycloak';
import { InviteLink, InviteLinkSchema } from '~/types/invitelink.model';

export default defineEventHandler(async (event) => {
  const group = await useDirectKeycloak().getGroup(event.context.params!.group);
  return accessGroupAdminService(event, group, async (service) => {
    const inviteId = event.context.params!.invite;
    const inviteCurrent = await useDirectKeycloak().getInvite(group, inviteId);
    if (!inviteCurrent) {
      setResponseStatus(event, 404, 'Invite nicht gefunden');
      return;
    }
    const body = await readBody(event);
    if (InviteLinkSchema.isValidSync(body)) {
      const invite = body;
      await service.updateInvite(invite);
      return 'success';
    } else {
      setResponseStatus(event, 400, 'Body must conform to InviteLinkSchema');
    }
  });
});
