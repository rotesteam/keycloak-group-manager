import { useDirectKeycloak } from '~/composables/useDirectKeycloak';
import { InviteLinkSchema } from '~/types/invitelink.model';

export default defineEventHandler(async (event) => {
  const group = await useDirectKeycloak().getGroup(event.context.params!.group);
  return accessGroupAdminService(event, group, async (service) => {
    const body = await readBody(event);
    body.expireDate = new Date(body.expireDate);
    InviteLinkSchema.validateSync(body);
    if (InviteLinkSchema.isValidSync(body)) {
      const invite = body;
      await service.addInvite(invite);
      return 'success';
    } else {
      setResponseStatus(event, 400, 'Body must conform to InviteLinkSchema');
    }
  });
});
