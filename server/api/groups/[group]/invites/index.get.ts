import { useDirectKeycloak } from '~/composables/useDirectKeycloak';

export default defineEventHandler(async (event) => {
  const group = await useDirectKeycloak().getGroup(event.context.params!.group);
  if (!(await mayManageGroup(event.context.kcUser, group))) {
    setResponseStatus(event, 403);
    return;
  }
  return useDirectKeycloak().getInvites(group);
});
