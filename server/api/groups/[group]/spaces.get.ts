import { useDirectKeycloak } from '~/composables/useDirectKeycloak';

export default defineEventHandler(async (event) => {
  const group = await useDirectKeycloak().getGroup(event.context.params!.group);
  if (await mayViewGroup(event.context.kcUser, group)) {
    return getSpacesOfGroup(group.id);
  } else {
    setResponseStatus(event, 403);
  }
});
