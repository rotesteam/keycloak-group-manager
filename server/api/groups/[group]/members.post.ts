import { useDirectKeycloak } from '../../../../composables/useDirectKeycloak';
import { array, object, string } from 'yup';

const ChangeMembersSchema = object({
  addMembers: array(string().required()).optional(),
  removeMembers: array(string().required()).optional(),
});

export default defineEventHandler(async (event) => {
  const group = await useDirectKeycloak().getGroup(event.context.params!.group);
  return accessGroupAdminService(event, group, async (service) => {
    const body = await readBody(event);
    if (ChangeMembersSchema.isValidSync(body)) {
      await service.changeUsers(
        body.addMembers ?? [],
        body.removeMembers ?? [],
      );
      return 'success';
    } else {
      setResponseStatus(event, 400, 'Body has wrong shape');
    }
  });
});
