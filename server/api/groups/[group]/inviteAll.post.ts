import { useDirectKeycloak } from '../../../../composables/useDirectKeycloak';

export default defineEventHandler(async (event) => {
  const group = await useDirectKeycloak().getGroup(event.context.params!.group);

  return accessGroupAdminService(event, group, async (service) => {
    await service.inviteAll();
    return 'success';
  });
});
