import { useDirectKeycloak } from '../../../composables/useDirectKeycloak';

export default defineEventHandler(async (event) => {
  return await useDirectKeycloak().fetchGroupsWhereAdmin(event.context.kcId);
});
