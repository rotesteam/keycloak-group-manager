import { GroupMembership, KCGroup, MembershipType } from '~/types/groups.model';
import { useDirectKeycloak } from '../../../../composables/useDirectKeycloak';

function mapGroup(group: KCGroup): GroupMembership {
  return {
    id: group.id,
    name: group.name,
    path: group.path,
    membershipType: useDirectKeycloak().isCompositeParent(group)
      ? MembershipType.INDIRECT
      : MembershipType.DIRECT,
  };
}

export default defineEventHandler(async (event) => {
  return (await useDirectKeycloak().getGroupsOfUser(event.context.kcId)).map(
    mapGroup,
  );
});
