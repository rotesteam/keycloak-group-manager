export default defineEventHandler(async (event) => {
  return matrixBot.getManagableSpaces(event.context.kcId);
});
