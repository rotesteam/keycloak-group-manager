import { useDirectKeycloak } from '../../../composables/useDirectKeycloak';
import { maySearchUsers } from '~/server/utils/permissions';
export default defineEventHandler(async (event) => {
  if (!(await maySearchUsers(event.context.kcUser))) {
    setResponseStatus(event, 403);
    return;
  }
  if (!event.context?.params?.user) {
    setResponseStatus(event, 400);
    return 'User id missing from path';
  }
  return await useDirectKeycloak().getUser(event.context.params.user);
});
