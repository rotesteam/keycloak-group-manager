import { useDirectKeycloak } from '../../../composables/useDirectKeycloak';
import { maySearchUsers } from '~/server/utils/permissions';
export default defineEventHandler(async (event) => {
  const { max, first, search } = getQuery(event);

  if (!await maySearchUsers(event.context.kcUser)) {
    setResponseStatus(event, 403);
    return;
  }
  // first, max, search
  const users = await useDirectKeycloak().getUsers(
    [max, null].flat()[0] as number,
    [first, null].flat()[0] as number,
    [search, null].flat()[0] as string,
  );
  return users.map((user) => ({
    id: user.id,
    username: user.username,
    firstName: user.firstName,
    lastName: user.lastName,
  }));
});
