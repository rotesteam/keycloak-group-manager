import { useDirectKeycloak } from '../../../composables/useDirectKeycloak';

export default defineEventHandler(async (event) => {
  const { search } = getQuery(event);

  return await useDirectKeycloak().getUserCount(
    [search, null].flat()[0] as string,
  );
});
