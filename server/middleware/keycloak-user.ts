import { useFetch } from 'nuxt/app';
import { UserInfo } from '../../types/userInfo.model';

export default defineEventHandler(async (event) => {
  if (getHeader(event, 'Authorization')) {
    const accessToken = getHeader(event, 'Authorization');
    const config: any = useRuntimeConfig();
    const response = await fetch(
      `${config.public.authIssuer}/protocol/openid-connect/userinfo`,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'bearer ' + accessToken,
        },
      },
    );
    if (response.status != 200) {
      console.error(
        'Error while fetching user of access token: ' + response.statusText,
      );
    } else {
      const data = (await response.json()) as UserInfo;

      event.context.kcId = data.sub;
      event.context.kcUser = data;
    }
  }
  function isInviteClaimInfo() {
    const pathMatches =
      /^\/api\/groups\/[a-zA-Z0-9\-]+\/invites\/[a-zA-Z0-9\-]+\/claiminfo$/.test(
        event.path,
      );
    const methodMatches = event.method == 'GET';
    return pathMatches && methodMatches;
  }
  if (
    (event.path.startsWith('/api/groups/') ||
      event.path.startsWith('/api/users')) &&
    !event.context.kcId &&
    !isInviteClaimInfo()
  ) {
    setResponseStatus(event, 403, 'auth required');
    return 'auth required'; // stop request
  }
});
