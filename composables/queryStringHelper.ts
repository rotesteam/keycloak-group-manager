export function createQueryString(
  params: Record<string, string | undefined | null>,
) {
  const urlQuery = new URLSearchParams();

  for (const [key, value] of Object.entries(params)) {
    if (value != undefined) {
      urlQuery.append(key, value);
    }
  }

  const query = urlQuery.toString();
  return query ? '?' + query : '';
}
