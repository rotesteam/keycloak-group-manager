import type { InviteLink } from '~/types/invitelink.model';

export function inviteLinkIsOk(invite: InviteLink): boolean {
  if (invite.usesLeft > 500 || invite.usesLeft < 0) {
    return false;
  }
  if (
    !invite.expireDate ||
    (!((invite.expireDate as any) instanceof Date) &&
      typeof invite.expireDate != 'string')
  ) {
    return false;
  }
  if (typeof invite.usesLeft != 'number') {
    return false;
  }
  if (typeof invite.useCount != 'number') {
    return false;
  }
  if (
    !invite.label ||
    typeof invite.label != 'string' ||
    invite.label.length >= 100
  ) {
    return false;
  }
  if (typeof invite.notifications != 'boolean') {
    return false;
  }
  return true;
}

export async function checkInviteLinkStatus(
  invite: InviteLink,
  isMemberChecker: () => Promise<boolean | undefined>,
): Promise<LinkStatus> {
  if (await isMemberChecker()) {
    return LinkStatus.ALREADY_MEMBER;
  }
  if (invite.usesLeft <= 0) {
    return LinkStatus.USES_EXHAUSTED;
  }
  if (new Date(invite.expireDate) < new Date()) {
    return LinkStatus.EXPIRED;
  }
  return LinkStatus.CLAIMABLE;
}

export enum LinkStatus {
  CLAIMABLE,
  EXPIRED,
  USES_EXHAUSTED,
  ALREADY_MEMBER,
}
