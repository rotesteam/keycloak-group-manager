import type { InviteLink } from '~/types/invitelink.model';
import { GroupMember } from '../types/group-member.model';
import { KCGroup } from '~/types/groups.model';
import { User } from '../types/user.model';
import { inviteLinkIsOk } from './inviteLink';
import { createQueryString } from './queryStringHelper';

const config = useRuntimeConfig();

type AttributeContent = string | { id?: string };
function attributeContentEqual(a: AttributeContent, b: AttributeContent) {
  return (
    a == b || ((a as any).id && (b as any).id && (a as any).id == (b as any).id)
  );
}

function getAttribute<T>(group: KCGroup, attribute: string): T[] {
  const vals = group.attributes?.[attribute] ?? [];
  const arr: T[] = vals.flatMap((val: string) => JSON.parse(val) ?? []);
  return arr;
}

function formatExpireDate(date: Date | string): string {
  date = new Date(date);
  const year = date.getFullYear();
  const month = String(date.getMonth() + 1).padStart(2, '0'); // Months are zero-based
  const day = String(date.getDate()).padStart(2, '0');

  return `${year}-${month}-${day}`;
}

const ADMIN_ATTRIBUTE = 'GROUP_ADMINS';
const SPACES_ATTRIBUTE = 'MATRIX_SPACES';
const INVITES_ATTRIBUTE = 'INVITELINKS';
const COMPOSITE_CHILD_ATTRIBUTE = 'COMPOSITE_CHILD';
const COMPOSITE_PARENT_ATTRIBUTE = 'COMPOSITE_PARENT';

function dateInSeconds(seconds: number) {
  const date = new Date();
  date.setSeconds(date.getSeconds() + seconds);
  return date;
}

class DirectKeycloak {
  constructor(private accessTokenProvider: AccessTokenProvider) {}

  private getAccessToken(): Promise<string> {
    return this.accessTokenProvider.getToken();
  }

  public async getKCHeader() {
    return {
      Authorization: `Bearer ${await this.getAccessToken()}`,
    };
  }
  private async fetchResource<T>(
    resource: string,
    method: 'GET' | 'POST' | 'PUT' | 'DELETE' = 'GET',
    body: any = null,
  ): Promise<T> {
    return await $fetch<T>(config.public.keycloakAdminApi + '/' + resource, {
      method,
      body,
      headers: {
        ...(await this.getKCHeader()),
        ...(body ? { 'Content-Type': 'application/json' } : {}),
      },
    });
  }

  private async fetchResourceExhaustively(
    resource: string,
    queryArgs: { [index: string]: any } = {},
  ): Promise<any[]> {
    // Initialise with sane defaults
    queryArgs = {
      first: 0, // Actually an offset
      max: 100,
      ...queryArgs,
    };
    const response = await this.fetchResource(
      resource + createQueryString(queryArgs),
    );
    if (Array.isArray(response)) {
      if (response.length == queryArgs.max) {
        const nextOffset = parseInt(queryArgs['first']) + 1;
        response.push(
          await this.fetchResourceExhaustively(resource, {
            ...queryArgs,
            first: nextOffset,
          }),
        );
      }
      return response;
    } else {
      throw new Error(
        `Expecting an array to be returned from ${resource} while fetching the resource exhaustively, but received: ${response}`,
      );
    }
  }

  public async joinGroup(
    keycloakMemberid: string,
    groupId: string,
  ): Promise<any> {
    return this.fetchResource(
      `/users/${encodeURIComponent(
        keycloakMemberid,
      )}/groups/${encodeURIComponent(groupId)}`,
      'PUT',
    );
  }

  public async leaveGroup(
    keycloakMemberid: string,
    groupId: string,
  ): Promise<any> {
    return this.fetchResource(
      `/users/${encodeURIComponent(
        keycloakMemberid,
      )}/groups/${encodeURIComponent(groupId)}`,
      'DELETE',
    );
  }

  public getUser(userId: string): Promise<User> {
    return this.fetchResource<User>(`/users/${encodeURIComponent(userId)}`);
  }

  public async fetchGroupsWhereAdmin(kcId: string) {
    const data = await this.fetchResource<KCGroup[]>(
      `/users/${encodeURIComponent(kcId)}/groups?briefRepresentation=false`,
    );
    const adminGroups: string[] = [];
    for (let group of data ?? []) {
      if (await this.isAdmin(kcId, group)) {
        adminGroups.push(group.id);
      }
    }
    return adminGroups;
  }

  public async getGroup(groupId: string) {
    return await this.fetchResource<KCGroup>(
      `groups/${encodeURIComponent(groupId)}`,
    );
  }

  public async getRootGroups(): Promise<KCGroup[]> {
    const groups: KCGroup[] = await this.fetchResourceExhaustively('/groups', {
      briefRepresentation: false,
      populateHierarchy: true,
    });
    return groups;
  }

  public async getChildrenGroups(groupId: string): Promise<KCGroup[]> {
    return this.fetchResourceExhaustively(
      `/groups/${encodeURIComponent(groupId)}/children`,
      { briefRepresentation: false },
    );
  }

  public async isAdmin(kcId: string, group: KCGroup) {
    const listedAsAdmin =
      this.getAdminsRaw(group).filter((admin) => admin == kcId).length > 0;
    return (
      listedAsAdmin &&
      (await this.getGroupsOfUser(kcId)).filter((g) => g.id == group.id)
        .length > 0
    );
  }

  public isMember(kcId: string, groupId: string): Promise<boolean> {
    return this.getGroupMembers(groupId).then((members) =>
      members.some((mem) => mem.id == kcId && kcId),
    );
  }

  /**
   * Returns the effective admins of the group, so those listed in the attribute and that are actually members
   * @param group
   * @returns
   */
  public async getAdmins(group: KCGroup): Promise<string[]> {
    const rawAdmins = this.getAdminsRaw(group);
    const memberIds = new Set(
      (await this.getGroupMembers(group.id)).map((u) => u.id),
    );
    return rawAdmins.filter((admin) => memberIds.has(admin));
  }

  public getCompositeChildren(group: KCGroup): string[] {
    return getAttribute(group, COMPOSITE_CHILD_ATTRIBUTE);
  }

  public async addCompositeChild(parent: KCGroup, child: KCGroup) {
    this.addToAttribute(parent, COMPOSITE_CHILD_ATTRIBUTE, child.id);
    await this.saveAttributes(parent);
  }

  public async removeCompositeChild(parent: KCGroup, child: KCGroup) {
    this.removeFromAttribute(parent, COMPOSITE_CHILD_ATTRIBUTE, child.id);
    await this.saveAttributes(parent);
  }

  public isCompositeParent(group: KCGroup): boolean {
    return getAttribute(group, COMPOSITE_CHILD_ATTRIBUTE).length > 0;
  }

  public async clearIsCompositeParent(group: KCGroup) {
    this.clearAttribute(group, COMPOSITE_CHILD_ATTRIBUTE);
    await this.saveAttributes(group);
  }

  public getCompositeParents(group: KCGroup): string[] {
    return getAttribute(group, COMPOSITE_PARENT_ATTRIBUTE);
  }

  public async removeCompositeParent(child: KCGroup, parent: KCGroup) {
    this.removeFromAttribute(child, COMPOSITE_PARENT_ATTRIBUTE, parent.id);
    await this.saveAttributes(child);
  }

  public async addCompositeParent(child: KCGroup, parent: KCGroup) {
    this.addToAttribute(child, COMPOSITE_PARENT_ATTRIBUTE, parent.id);
    await this.saveAttributes(child);
  }

  private getAdminsRaw(group: KCGroup): string[] {
    return getAttribute(group, ADMIN_ATTRIBUTE);
  }

  private updateInAttribute(
    group: KCGroup,
    attributeName: string,
    obj: AttributeContent,
  ) {
    this.removeFromAttribute(group, attributeName, obj);
    this.addToAttribute(group, attributeName, obj);
  }

  private addToAttribute(
    group: KCGroup,
    attributeName: string,
    obj: AttributeContent,
  ) {
    group.attributes ??= {};
    const currentVals: AttributeContent[] = getAttribute(group, attributeName);
    if (!currentVals.find((o) => attributeContentEqual(o, obj))) {
      currentVals.push(obj);
    }
    group.attributes[attributeName] = currentVals.map((elem) =>
      JSON.stringify(elem),
    );
  }

  private removeFromAttribute(
    group: KCGroup,
    attributeName: string,
    obj: AttributeContent,
  ) {
    group.attributes ??= {};
    const currentVals: AttributeContent[] = getAttribute(group, attributeName);
    let newVals = currentVals.filter(
      (objInAttribute) => !attributeContentEqual(objInAttribute, obj),
    );
    group.attributes[attributeName] = newVals.map((elem) =>
      JSON.stringify(elem),
    );
  }

  private clearAttribute(group: KCGroup, attributeName: string) {
    group.attributes ??= {};
    group.attributes[attributeName] = [];
  }

  public changeAdmins(
    group: KCGroup,
    adminsToAdd: string[],
    adminsToRemove: string[],
  ): Promise<void> {
    for (const newAdmin of adminsToAdd) {
      this.addToAttribute(group, ADMIN_ATTRIBUTE, newAdmin);
    }
    for (const deleteAdmin of adminsToRemove) {
      this.removeFromAttribute(group, ADMIN_ATTRIBUTE, deleteAdmin);
    }
    return this.saveAttributes(group);
  }

  public changeSpaces(
    group: KCGroup,
    spacesToAdd: string[],
    spacesToRemove: string[],
  ): Promise<void> {
    spacesToAdd.forEach((space) =>
      this.addToAttribute(group, SPACES_ATTRIBUTE, space),
    );
    spacesToRemove.forEach((space) =>
      this.removeFromAttribute(group, SPACES_ATTRIBUTE, space),
    );
    return this.saveAttributes(group);
  }

  public async getInvite(
    group: KCGroup,
    inviteId: string,
  ): Promise<InviteLink | null> {
    return (
      (await this.getInvites(group)).find(
        (invite) => inviteId && invite.id == inviteId,
      ) ?? null
    );
  }

  public async getInvites(group: KCGroup): Promise<InviteLink[]> {
    const invites: InviteLink[] = getAttribute(group, INVITES_ATTRIBUTE);
    return invites;
  }

  public async addInvite(group: KCGroup, invite: InviteLink) {
    const { randomUUID } = await import('crypto');
    invite.id = randomUUID();
    invite.useCount = 0;
    if (!inviteLinkIsOk(invite)) {
      throw new Error('Der Einladungslink ist nicht valide');
    }
    (invite as any).expireDate = formatExpireDate(invite.expireDate);
    this.addToAttribute(group, INVITES_ATTRIBUTE, invite);
    await this.saveAttributes(group);
  }

  public async updateInvite(group: KCGroup, invite: InviteLink) {
    if (!inviteLinkIsOk(invite)) {
      throw new Error('Der Einladungslink ist nicht valide');
    }
    (invite as any).expireDate = formatExpireDate(invite.expireDate);
    this.updateInAttribute(group, INVITES_ATTRIBUTE, invite);
    await this.saveAttributes(group);
  }

  public async deleteInvite(group: KCGroup, invite: InviteLink) {
    this.removeFromAttribute(group, INVITES_ATTRIBUTE, invite);
    await this.saveAttributes(group);
  }

  public async getUsers(
    max: number | null = null,
    first: number | null = null,
    search: string | null = null,
  ): Promise<User[]> {
    const queryString = createQueryString({
      max: max?.toString(),
      first: first?.toString(),
      search: search,
    });

    return await this.fetchResource<User[]>(`/users/` + queryString);
  }

  public async getUserCount(search: string | null = null): Promise<number> {
    const queryString = createQueryString({ search: search });
    return await this.fetchResource<number>(`/users/count` + queryString);
  }

  public async getGroupsOfUser(kcId: string): Promise<KCGroup[]> {
    if (!kcId) {
      throw new Error('kcId must be passed');
    }
    return await this.fetchResource<KCGroup[]>(
      `/users/${kcId}/groups?briefRepresentation=false`,
    );
  }

  public async getGroupMembers(groupId: string): Promise<GroupMember[]> {
    return await this.fetchResourceExhaustively(`/groups/${groupId}/members`);
  }

  public async getMembers(groupIds: string[]) {
    const groups = (
      await Promise.all(groupIds.map((id) => this.getGroup(id)))
    ).filter((g) => !!g);
    const members: Map<string, Set<string>> = new Map();
    for (const group of groups) {
      members.set(
        group.id,
        new Set((await this.getGroupMembers(group.id)).map((u) => u.id)),
      );
    }
    return {
      groups,
      members,
      getUnifiedMembers: () =>
        new Set(
          Array.from(members.values()).flatMap((userSet) =>
            Array.from(userSet.values()),
          ),
        ),
    };
  }

  public async saveAttributes(group: KCGroup): Promise<void> {
    return this.fetchResource<any>(`groups/${group.id}`, 'PUT', group);
  }

  public async isAdminSomewhere(kcId: string): Promise<boolean> {
    return (await this.fetchGroupsWhereAdmin(kcId)).length > 0;
  }
}
interface AccessTokenProvider {
  getToken(): Promise<string>;
}

class ClientSecretTokenProvider implements AccessTokenProvider {
  private token: string | null = null;
  private validUntil: Date = new Date(0);
  private tokenPromise: null | Promise<string> = null;

  async getToken() {
    if (this.validUntil > dateInSeconds(1) && this.token) {
      return this.token;
    } else if (this.tokenPromise) {
      return this.tokenPromise;
    } else {
      this.tokenPromise = (async () => {
        const url = `${config.public.authIssuer}/protocol/openid-connect/token`;
        const response = await fetch(url, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
          },
          body: `grant_type=client_credentials&client_id=${encodeURIComponent(
            config.authClientId!,
          )}&client_secret=${encodeURIComponent(config.authSecret!)}`,
        });
        if (response.status != 200) {
          throw new Error(
            `Error response ${response.status} - ${
              response.statusText
            } while fetching ${url}. Response-body: ${await response.text()}`,
          );
        }
        const data = await response.json();
        if (!data.access_token) {
          throw new Error('No access token in response');
        }

        this.token = data.access_token;
        this.validUntil = dateInSeconds(data.expires_in);
        return this.token!;
      })();
      this.tokenPromise.catch((err) =>
        console.error('Error during fetching of token', err),
      );
      this.tokenPromise.finally(() => {
        this.tokenPromise = null;
      });
      return this.tokenPromise;
    }
  }
}

class FrontendAuthTokenProvider implements AccessTokenProvider {
  async getToken(): Promise<string> {
    const token = useKeycloakHelper().getAccessToken();
    if (!token) {
      throw new Error('Konnte den Access token nicht holen');
    }
    return token;
  }

  static isAvailable(): boolean {
    return !!useKeycloakHelper().getAccessToken();
  }
}

let instance: DirectKeycloak | null;
export function useDirectKeycloak() {
  if (instance) {
    return instance;
  }
  if (config.authSecret && config.authClientId) {
    instance = new DirectKeycloak(new ClientSecretTokenProvider());
  } else if (FrontendAuthTokenProvider.isAvailable()) {
    instance = new DirectKeycloak(new FrontendAuthTokenProvider());
  } else {
    throw new Error('Konnte DirectKeycloak nicht erstellen');
  }
  return instance;
}
