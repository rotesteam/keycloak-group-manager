import * as yup from 'yup';

export const InviteLinkSchema = yup.object({
  id: yup.string().defined().strict(true),
  expireDate: yup.date().required(),
  usesLeft: yup.number().required().integer().min(0),
  useCount: yup.number().required().integer().min(0),
  notifications: yup.boolean().required(),
  label: yup.string().required(),
});

export type InviteLink = yup.InferType<typeof InviteLinkSchema>;

export type ClaimInfo = {
  status: ClaimStatus;
  groupName: string;
};

export enum ClaimStatus {
  CLAIMABLE,
  ALREADY_MEMBER,
  NOTCLAIMABLE_OR_MISSING,
}
