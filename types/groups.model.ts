export type KCGroup = {
  id: string;
  name: string;
  path: string;
  attributes?: { [key: string]: string[] };
  subGroups?: KCGroup[];
  subGroupCount?: number;
};
export enum MembershipType {
  DIRECT,
  INDIRECT,
}
export type GroupMembership = {
  id: string;
  name: string;
  path: string;
  membershipType: MembershipType;
};

export interface GroupInfo {
  id: string;
  name: string;
  path: string;
  compositeGroup: boolean;
}
