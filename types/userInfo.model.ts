export type UserInfo = {
  id: string;
  group_membership: string[];
  sub: string;
  resource_access: ResourceAccess;
  email_verified: boolean;
  realm_access: {
    roles: string[];
  };
  preferred_username: string;
  given_name: string;
  family_name: string;
  email: string;
};

export type ResourceAccess = {
  account: {
    roles: string[];
  };
  'realm-management': {
    roles: string[];
  };
};
